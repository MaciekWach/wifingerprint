.. aaaa documentation master file, created by
   sphinx-quickstart on Tue Sep 12 17:15:54 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to aaaa's documentation!
================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   GetNetworkInfoWindows
   SaveMeasure
   SaveMeasure_xml
   WiFingerprint


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
