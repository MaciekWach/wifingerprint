import ctypes
import datetime
# system language -> supported options
# .---------------------.----------.
# | Languages           | Value    |
# |---------------------|----------|
# | en_US               |   1033   |
# | en_GB               |   2057   |
# | pl_PL               |   1045   |
# '---------------------'----------'
class SaveMeasure():
    """
    Class has methods responsible for save final values to .txt file.
    """
    @staticmethod
    def search(dictionary, substr):
        """
        Method responsible for search value in dict based on substring
        :param dictionary:
        :param substr:
        :return: searched value
        """
        result = []
        for key in dictionary:
            if substr in key:
                result.append((key, dictionary[key]))
        return result

    @staticmethod
    def save_me(dict, path):
        """
        Main method which is responsible for save final dictionary to .txt file.
        Only information about: SSID, BSSID, Signal Strength and Measure Date are saved.
        :param dict:
        :param path:
        :return: .txt file is created as result
        """
        print dict
        date = str(datetime.datetime.now().strftime("%d-%m-%Y %H:%M"))
        windows_language_number = ctypes.windll.kernel32.GetUserDefaultUILanguage()
        if windows_language_number in (1033, 2057):
            signal = "Signal"
        elif windows_language_number == 1045:
            signal = "Sygna\x88"
        else:
            print "UNSUPPORTED LANGUAGE"
        with open(path + ".txt", "w") as text_file:
            for point in dict:
                text_file.write("Measurement Date: " + date + "\n")
                text_file.write( "#"*50+"\n")
                text_file.write(str(point)+"\n")
                for SSID in dict[point]:
                    text_file.write( "-"*50+"\n")
                    text_file.write( SSID[0]+" : "+ SSID[1]+"\n")
                    for BSSID in [k for k in SaveMeasure.search(dict[point][SSID], "BSSID")]:
                            text_file.write( BSSID[0]+" : "+BSSID[1]+"\n")
                    text_file.write("Signal Strength" + " : " + str(dict[point][SSID][signal]) + "\n")
                    text_file.write("dBm" + " : " + str(dict[point][SSID]['dBm']) + "\n")

if __name__ == "__main__":
    s = SaveMeasure()