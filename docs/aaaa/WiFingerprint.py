# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class filedialogdemo(QtGui.QWidget):
    """
    Class responsible for creation the OpenFile window
    """
    def __init__(self, parent=None):
        super(filedialogdemo, self).__init__(parent)
    def getfile(self):
        """
        Method responsible for display "Load Window"
        Accepted formats: .jpg .gif. png
        """
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Open file',
                                            'c:\\', "Image files (*.jpg *.gif *.png)")
        return fname

class Ui_MainWindow(object):
    """
    Main Class in project responsilbe for GUI creation and for logical operations on it.
    """
    def __init__(self):
        """Declaration of final dictionary with point in constructor"""
        self.networks = None
        self.points = {}

    def setupUi(self, MainWindow):
        """
        Main GUI Method responsible for creation MainWindow, all buttons and views.
        :param MainWindow:
        :return: None
        """
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(970, 587)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        MainWindow.setFont(font)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.graphicsView = QtGui.QGraphicsView(self.centralwidget)
        self.graphicsView.setGeometry(QtCore.QRect(240, 10, 1200, 700))
        self.graphicsView.setMaximumSize(QtCore.QSize(1200, 700))
        self.graphicsView.setObjectName(_fromUtf8("graphicsView"))
        self.Measure_button = QtGui.QPushButton(self.centralwidget)
        self.Measure_button.setGeometry(QtCore.QRect(40, 60, 75, 23))
        self.Measure_button.setCheckable(True)
        self.Measure_button.setChecked(False)
        font = QtGui.QFont()
        font.setPointSize(8)
        self.Measure_button.setFont(font)
        self.Measure_button.setObjectName(_fromUtf8("Measure_button"))
        self.Measure_button.setStyleSheet("background-color: red")
        self.Save_button = QtGui.QPushButton(self.centralwidget)
        self.Save_button.setGeometry(QtCore.QRect(40, 100, 75, 23))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.Save_button.setFont(font)
        self.Save_button.setObjectName(_fromUtf8("Save_button"))
        self.Load_button = QtGui.QPushButton(self.centralwidget)
        self.Load_button.setGeometry(QtCore.QRect(40, 140, 75, 23))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.Load_button.setFont(font)
        self.Load_button.setObjectName(_fromUtf8("Load_button"))
        self.Close_button = QtGui.QPushButton(self.centralwidget)
        self.Close_button.setGeometry(QtCore.QRect(40, 180, 75, 23))
        font = QtGui.QFont()
        font.setPointSize(8)
        self.Close_button.setFont(font)
        self.Close_button.setObjectName(_fromUtf8("Close_button"))
        self.calendarWidget = QtGui.QCalendarWidget(self.centralwidget)
        self.calendarWidget.setGeometry(QtCore.QRect(10, 320, 216, 155))
        self.calendarWidget.setObjectName(_fromUtf8("calendarWidget"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(50, 20, 150, 16))
        self.label.setObjectName(_fromUtf8("label"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 970, 23))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.palette = QtGui.QPalette()
        # SET MENUBAR FUNCTIONS
        openFile = QtGui.QAction("&Open File", self.menubar)
        openFile.setShortcut("Ctrl+O")
        openFile.setStatusTip('Open File')
        openFile.triggered.connect(self.load)
        saveFile = QtGui.QAction("&Save File", self.menubar)
        saveFile.setShortcut("Ctrl+S")
        saveFile.setStatusTip('Save File')
        saveFile.triggered.connect(self.save)
        close = QtGui.QAction("&Close", self.menubar)
        close.setStatusTip('Close')
        close.triggered.connect(self.close)

        fileMenu = self.menubar .addMenu('&MENU')
        fileMenu.addAction(openFile)
        fileMenu.addAction(saveFile)
        fileMenu.addAction(close)
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        """Method responsible for setting text and onclick behaviuor to buttons, anf for set welcome graphic"""
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.Measure_button.setText(_translate("MainWindow", "MEASURE", None))
        self.Save_button.setText(_translate("MainWindow", "Save", None))
        self.Load_button.setText(_translate("MainWindow", "Load", None))
        self.Close_button.setText(_translate("MainWindow", "Close", None))
        self.palette.setColor(QtGui.QPalette.Foreground, QtCore.Qt.red)
        self.label.setPalette(self.palette)
        self.label.setText(_translate("MainWindow", "MEASUREMENT OFF", None))
        self.set_image_in_view(os.getcwd() + '\\' + 'welcome.png')
        self.Measure_button.clicked.connect(self.set_status)
        self.Load_button.clicked.connect(self.load)
        self.Close_button.clicked.connect(self.close)
        self.Save_button.clicked.connect(self.save)
        # self.graphicsView.mousePressEvent = self.getPosition
        # self.pixmap_item.mousePressEvent = self.getPosition

    def set_image_in_view(self, pixmap_path):
        """
        Method is responsible for set image in PyQt view
        :param pixmap_path: path to PyQt pixmap
        :return: None
        """
        self.scene = QtGui.QGraphicsScene(self.centralwidget)
        self.image = QtGui.QPixmap(pixmap_path)
        self.pixmap_item = QtGui.QGraphicsPixmapItem(self.image, None, self.scene)
        self.scene.addPixmap(self.image)
        self.graphicsView.setScene(self.scene)
        self.pixmap_item.mousePressEvent = self.getPosition

####################################### GUI LOGIC #########################################

    def save(self):
        """
        Method is responsible for saving final output
        this method use @Staticmethods from SaveMeasure and SaveMeasure_xml modules
        :return: None
        """
        try:
            self.Measure_button.setChecked(False)
            self.set_status()
            name = QtGui.QFileDialog.getSaveFileName(self.menubar, 'Save File', 'default')
            name = str(name)
            if not os.path.exists(name):
                os.makedirs(name)
            p = self.image
            dict_name = re.search(r'[\w\d]+$',name).group()
            path = str(name)+'/'+dict_name
            p.save(path + ".jpg")
            SaveMeasure_xml.save_me(self.points, path=path)
            SaveMeasure.save_me(self.points, path=path)
        except WindowsError:
            print "WINDOWS ERROR"

    def close(self):
        """
        Method is responsible for handle 'Close' event
        triggered by 'Close' button
        :return: None
        """
        if self.points:
            value = self.showdialog("close")
            if value == 16384:
                self.points = {}
                MainWindow.close()
        else:
            MainWindow.close()

    def load(self):
        """
        Method is responsible for hadle 'Load' event
        and for display proper windows
        :return: None
        """
        try:
            print self.pixmap_item
            if self.points:
                value = self.showdialog("load")
                if value == 16384:
                    self.points = {}
                    self.Measure_button.setChecked(False)
                    self.set_status()
                    read = filedialogdemo().getfile()
                    self.set_image_in_view(read)
            else:
                self.points = {}
                self.Measure_button.setChecked(False)
                read = filedialogdemo().getfile()
                self.set_image_in_view(read)
            print self.pixmap_item
        except WindowsError:
            print "WINDOWS ERROR"

    def getPosition(self, event):
        """
        IMPORTANT!
        Method is responsible for:
        - handle x,y co-ordinates
        - create Ellipse marker and add marker to image
        - create final response in dictionary
        :param event:
        :return: add registry to dict
        """
        if self.Measure_button.isChecked():
            x = event.pos().x()
            y = event.pos().y()
            pen = QtGui.QPen(QtCore.Qt.red)
            paint = QtGui.QPainter(self.image)
            paint.setPen(pen)
            paint.drawEllipse(x+4,y-4,-8,8)
            self.scene.addPixmap(self.image)
            self.networks = GetNetworkInfo()
            self.networks.get_network_info()
            self.points[(x, y)] = self.networks.final_dict
            print self.points

    def set_status(self):
        """
        Method is responsible for set color of 'Measure" button and text field
        depends on MeasureButton status
        :return: None
        """
        if not self.Measure_button.isChecked():
            self.palette.setColor(QtGui.QPalette.Foreground, QtCore.Qt.red)
            self.label.setPalette(self.palette)
            self.graphicsView.viewport().setProperty("cursor", QtGui.QCursor(QtCore.Qt.ArrowCursor))
            self.label.setText(_translate("MainWindow", "MEASUREMENT OFF", None))
            self.Measure_button.setStyleSheet("background-color: red")
        else:
            self.palette.setColor(QtGui.QPalette.Foreground, QtCore.Qt.green)
            self.graphicsView.viewport().setProperty("cursor", QtGui.QCursor(QtCore.Qt.CrossCursor))
            self.label.setPalette(self.palette)
            self.label.setText(_translate("MainWindow", "MEASUREMENT ON", None))
            self.Measure_button.setStyleSheet("background-color: green")


    def showdialog(self, reason):
        """
        Method is responsible for display confirmation MessageBox
        :param reason: load / close - differential in description
        :return:  value of pressed MessageBox
        """
        self.msg = QtGui.QMessageBox()
        self.msg.setIcon(QtGui.QMessageBox.Information)
        self.msg.setText("Do you want to discard actual measurement ?")
        self.msg.setWindowTitle("Confirmation")
        if reason == "load":
            self.msg.setDetailedText("1. Measurement will be discarded. \n"
                                     "2. All actual points will be removed from database. \n"
                                     "3. You will be able to load new image and perform new measurement.")
        elif reason == "close":
            self.msg.setDetailedText("1. Measurement will be discarded. \n"
                                     "2. All actual points will be removed from database. \n"
                                     "3. Program will be closed.")
        self.msg.setStandardButtons(QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        self.msg.buttonClicked.connect(self.msgbtn)
        retval = self.msg.exec_()
        # print "value of pressed message box button:", retval
        return retval

class MyWindow(QtGui.QMainWindow):
    """
    This class is responsible for handle 'exit' event
    and for display confirmation window after click on the red button
    """
    def closeEvent(self,event):
        result = QtGui.QMessageBox.question(self,
                                            "Confirm Exit...",
                                            "Are you sure you want to exit ?",
                                            QtGui.QMessageBox.Yes| QtGui.QMessageBox.No)
        event.ignore()

        if result == QtGui.QMessageBox.Yes:
            event.accept()

if __name__ == "__main__":
    import sys
    print sys.platform
    if sys.platform == "win32":
        from GetNetworkInfoWindows import GetNetworkInfo
        from SaveMeasure_xml import SaveMeasure_xml
        from SaveMeasure import SaveMeasure
        import os
        import re
        app = QtGui.QApplication(sys.argv)
        # MainWindow = QtGui.QMainWindow()
        MainWindow = MyWindow()
        ui = Ui_MainWindow()
        ui.setupUi(MainWindow)
        MainWindow.showMaximized()
        sys.exit(app.exec_())
    elif sys.platform == "linux2":
        pass
    else:
        print "UNSUPPORTED OS!"

# sys.platform -> ALL VALUES
# .---------------------.----------.
# | System              | Value    |
# |---------------------|----------|
# | Linux (2.x and 3.x) | linux2   |
# | Windows             | win32    |
# | Windows/Cygwin      | cygwin   |
# | Mac OS X            | darwin   |
# | OS/2                | os2      |
# | OS/2 EMX            | os2emx   |
# | RiscOS              | riscos   |
# | AtheOS              | atheos   |
# | FreeBSD 7           | freebsd7 |
# | FreeBSD 8           | freebsd8 |
# '---------------------'----------'
